from random import randint
import numpy as np
import torch
import nltk
nltk.download('punkt')


## models can be downloaded at https://github.com/facebookresearch/InferSent


def BLSTM_cos(sent1, sent2):
    
    #GPU Model. uncomment this line comment all others.
    #model = torch.load('infersent.allnli.pickle')

    #torch.load(..pickle) will use GPU/Cuda by default. If you are on CPU:
    model = torch.load('infersent.allnli.pickle', map_location=lambda storage, loc: storage)
    model.use_cuda = False
    
    GLOVE_PATH = '../dataset/GloVe/glove.840B.300d.txt'

    model.set_glove_path(GLOVE_PATH)

    model.build_vocab_k_words(K=5000000)

   
    
    def cosine(u, v):
        return np.dot(u, v) / (np.linalg.norm(u) * np.linalg.norm(v)) 
    
    
    
    
    
    
    return cosine(model.encode([sent1])[0], model.encode([sent2])[0])
